//
//  ViewController.swift
//  SampleRecording
//
//  Created by PhuongTHN-D1 on 8/26/19.
//  Copyright © 2019 PhuongTHN-D1. All rights reserved.
//

import UIKit
import AVFoundation
import CoreML

class ViewController: UIViewController {
    private let audioEngine = AVAudioEngine()
    var bufferSize = 4800
    var sampleRate: Double = 16000
    
    var resultData: String = ""
    var recordBuffer: [Double] = Array(repeating: 0.0, count: 16000)

    let model = pigo_ios_v4_99318()
    
    @IBOutlet weak var recordButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        recordButton.isEnabled = false
        
        DispatchQueue.global(qos: .background).async {
            while true {
                self.runModel(onBuffer: self.recordBuffer)
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        AVAudioSession.sharedInstance().requestRecordPermission { granted in
            if granted {
                DispatchQueue.main.async {
                    self.recordButton.isEnabled = true
                }
            } else {
                self.recordButton.setTitle("Not Available", for: [])
            }
        }
    }
    
    @IBAction func recordingButtonTapped(_ sender: Any) {
        if audioEngine.isRunning {
            audioEngine.stop()
            recordButton.isEnabled = false
            recordButton.setTitle("Stopped", for: .disabled)
        } else {
            do {
                try startRecording()
                recordButton.setTitle("Stop Recording", for: [])
            } catch {
                recordButton.setTitle("Recording Not Available", for: [])
            }
        }
    }
    
    func startRecording() throws {
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setPreferredSampleRate(sampleRate)
        } catch {
            print(error.localizedDescription)
        }
        try audioSession.setCategory(.record, mode: .measurement, options: .duckOthers)
        try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
        
        let inputNode = audioEngine.inputNode
        let recordingFormat = inputNode.outputFormat(forBus: 0)

        inputNode.installTap(onBus: 0, bufferSize: AVAudioFrameCount(bufferSize), format: recordingFormat) { (buffer: AVAudioPCMBuffer, when: AVAudioTime) in
            let arraySize = Int(buffer.frameLength)
            let samples = Array(UnsafeBufferPointer(start: buffer.floatChannelData![0], count:arraySize))
            
            self.addItemRecord(newItems: samples)
        }
        
        audioEngine.prepare()
        try audioEngine.start()
    }
    
    func writeToFile(data: String, name: String) {
        let file = name
        
        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let fileURL = dir.appendingPathComponent(file)
            do {
                try data.write(to: fileURL, atomically: false, encoding: .utf8)
            }
            catch {
                debugPrint(error.localizedDescription)
            }
        }
    }
    
    func appendToResult(data: String) {
        resultData += data
    }
    
    private func addItemRecord(newItems: [Float]) {
        for i in newItems {
            let doubleI = Double(i)
            recordBuffer.append(doubleI)
            recordBuffer.remove(at: 0)
        }
    }
    
    private func runModel(onBuffer buffer: [Double]) {
        guard let maxElement: Double = buffer.max() else { return }
        let doubles = buffer.map { Double($0) / maxElement }
        
        let mfcc = MFCC()
        let afterMFCC = mfcc.processTemp(doubleInputBuffer: doubles)
        let transposedArray: [[Double]] = swapAxis(list: afterMFCC)
        let flattenArray = transposedArray.flatMap{$0}
        
        guard let arr = try? MLMultiArray(shape: [1, 101, 40], dataType: .double) else {
            fatalError("unable to crate mlarray")
        }
        
        for (index, element) in flattenArray.enumerated() {
            arr[index] = NSNumber(value: element)
        }
        
        let predictionOutput = try? model.prediction(input_1: arr, gru_1_h_in: nil, gru_2_h_in: nil)

        guard let result = predictionOutput?.dense_2_Softmax.doubleArray() else { return }
        let heyPigoConfidence = result[1]
        print("heyPigoConfidence : \(heyPigoConfidence)")

        var resultText = ""
        for i in buffer {
            resultText += "\(i),"
        }
        writeToFile(data: resultText, name: String(heyPigoConfidence))
        if heyPigoConfidence > 0.5 {
            print("heyPigoConfidence : \(heyPigoConfidence)")
            debugPrint("Sound Triggered")
        }
    }
    
    func swapAxis(list: [[Double]]) -> [[Double]] {
        let row = list.count
        let column = list[0].count
        
        var result: [[Double]] = [[Double]](repeating: [Double](repeating: 0.0, count: row), count: column)
        
        for i in 0..<column {
            for j in 0..<row {
                result[i][j] = list[j][i]
            }
        }
        return result
    }
    
    func readFromFile(fileName: String) -> [String]? {
        var arrayOfStrings: [String]?
        do {
            if let path = Bundle.main.path(forResource: fileName, ofType: "txt"){
                let data = try String(contentsOfFile:path, encoding: String.Encoding.utf8)
                arrayOfStrings = data.components(separatedBy: ",")
                return arrayOfStrings
            }
        } catch let err as NSError {
            print(err)
        }
        return arrayOfStrings
    }
}

@available(iOS 11.0, *)
extension MLMultiArray {
    public func doubleArray() -> [Double] {
        if self.count == 0 {
            return [Double]()
        }
        let ptr = self.dataPointer.bindMemory(to: Double.self, capacity: self.count)
        return Array(UnsafeBufferPointer(start: ptr, count: self.count))
    }
}

