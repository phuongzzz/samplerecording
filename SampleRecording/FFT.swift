//
//  FFT.swift
//  TetViet
//
//  Created by PhuongTHN-D1 on 8/8/19.
//  Copyright © 2019 Rikkeisoft. All rights reserved.
//

import Foundation

class FFT {
    var real: [Double] = []
    var imag: [Double] = []
    
    func proccess(signal: [Double]) {
        let numPoints: Int = signal.count
        real = signal
        imag = Array(repeating: 0.0, count: numPoints)
        
        let pi: Double = Double.pi
        let numStages: Int = Int((log(Double(numPoints)) / log(2)))
        let halfNumPoints = numPoints >> 1
        var j: Int = halfNumPoints
        
        var k: Int
        for i in 1..<(numPoints - 2) {
            if (i < j) {
                let tempReal: Double = real[j]
                let tempImag: Double = imag[j]
                real[j] = real[i]
                imag[j] = imag[i]
                real[i] = tempReal
                imag[i] = tempImag
            }
            k = halfNumPoints;
            while (k <= j) {
                j -= k
                k >>= 1
            }
            j += k
        }
        
        for stage in 1...numStages {
            var LE: Int = 1
            for _ in 0..<stage {
                LE <<= 1
            }
            
            let LE2: Int = LE >> 1
            var UR: Double = 1
            var UI: Double = 0
            
            let SR: Double = cos(pi / Double(LE2))
            let SI: Double = -sin(pi / Double(LE2))
            
            for subDFT in 1...LE2 {
                for butterfly in stride(from: (subDFT - 1), to: (numPoints - 1), by: LE) {
                    let ip: Int = butterfly + LE2
                    let tempReal: Double = Double(real[ip] * UR - imag[ip] * UI)
                    let tempImag: Double = Double(real[ip] * UI + imag[ip] * UR)
                    real[ip] = real[butterfly] - tempReal
                    imag[ip] = imag[butterfly] - tempImag
                    real[butterfly] += tempReal
                    imag[butterfly] += tempImag
                }
                let tempUR: Double = UR
                UR = tempUR * SR - UI * SI
                UI = tempUR * SI + UI * SR
            }
        }
    }
}
