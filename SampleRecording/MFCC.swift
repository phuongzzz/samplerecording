//
//  MFCC.swift
//  TetViet
//
//  Created by PhuongTHN-D1 on 8/8/19.
//  Copyright © 2019 Rikkeisoft. All rights reserved.
//

import Foundation

class MFCC {
    let n_mfcc: Int = 40
    let fMin: Double = 0.0
    let n_fft: Int = 512
    let hop_length: Int = 160
    let n_mels: Int = 40

    let sampleRate: Double = 16000.0

    var fMax: Double {
        return sampleRate / 2.0
    }

    let fft = FFT()

//    func process(doubleInputBuffer: [Double]) -> [Float] {
//        let mfccResult: [[Double]] = dctMfcc(y: doubleInputBuffer)
//        return finalshape(mfccSpecTro: mfccResult)
//    }

    func processTemp(doubleInputBuffer: [Double]) -> [[Double]] {
        return dctMfcc(y: doubleInputBuffer)
    }

    // NOT USED
//    func finalshape(mfccSpecTro: [[Double]]) -> [Float] {
//        var finalMfcc = [Float]()
//        var k: Int = 0
//        for i in 0..<mfccSpecTro[0].count {
//            for j in 0..<mfccSpecTro.count {
//                finalMfcc[k] = Float(mfccSpecTro[j][i])
//                k += 1
//            }
//        }
//        return finalMfcc
//    }

    func dctMfcc(y: [Double]) -> [[Double]] {
        let spectrogramInput = melSpectrogram(y: y)
        let specTroGram: [[Double]] = powerToDb(melS: spectrogramInput)
        let dctBasis: [[Double]] = dctFilter(n_filters: n_mfcc, n_input: n_mels)
        
        let a1 = n_mfcc
        let a2 = specTroGram[0].count
        var mfccSpecTro: [[Double]] = Array(repeating: [Double](repeating: 0.0, count: a2), count: a1)

        for i in 0..<n_mfcc {
            for j in 0..<specTroGram[0].count {
                for k in 0..<specTroGram.count {
                    mfccSpecTro[i][j] += dctBasis[i][k] * specTroGram[k][j]
                }
            }
        }
        return mfccSpecTro
    }

    func melSpectrogram(y: [Double]) -> [[Double]] {
        var melBasis: [[Double]] = melFilter()
        var spectro: [[Double]] = stftMagSpec(y: y)
        
        let a1 = melBasis.count
        let a2 = spectro[0].count
        var melS: [[Double]] = [[Double]](repeating: [Double](repeating: 0.0, count: a2), count: a1)
        
        for i in 0..<melBasis.count {
            for j in 0..<spectro[0].count {
                for k in 0..<melBasis[0].count {
                    melS[i][j] += melBasis[i][k] * abs(spectro[k][j])
                }
                melS[i][j] = pow(melS[i][j], 2)
            }
        }
        return melS
    }

    func stftMagSpec(y: [Double]) -> [[Double]] {
        let fftwin: [Double] = getWindow()
        
        let ypadSize = n_fft + y.count
        var ypad: [Double] = Array(repeating: 0, count: ypadSize)
        
        for i in 0..<(n_fft / 2) {
            ypad[(n_fft / 2) - i - 1] = y[i + 1]
            ypad[(n_fft / 2) + y.count + i] = y[y.count - 2 - i]
        }
        for j in 0..<y.count {
            ypad[(n_fft / 2) + j] = y[j]
        }

        let frame: [[Double]] = yFrame(ypad: ypad)
        
        
        let a1 = 1 + n_fft / 2
        let a2 = frame[0].count
        var fftmagSpec: [[Double]] = Array(repeating: [Double](repeating: 0.0, count: a2), count: a1)
        
        var fftFrame: [Double] = Array(repeating: 0.0, count: n_fft)

        for k in 0..<frame[0].count {
            for l in 0..<n_fft {
                fftFrame[l] = fftwin[l] * frame[l][k]
            }
            var magSpec: [Double] = magSpectrogram(frame: fftFrame)
            for i in 0..<(1 + n_fft/2) {
                fftmagSpec[i][k] = magSpec[i]
            }
        }
        return fftmagSpec
    }

    func magSpectrogram(frame: [Double]) -> [Double] {
        var magSpec: [Double] = Array(repeating: 0.0, count: frame.count)
        fft.proccess(signal: frame)

        for m in 0..<frame.count {
            magSpec[m] = fft.real[m]
        }
        return magSpec
    }
    
    func getWindow() -> [Double] {
        var win: [Double] = Array(repeating: 0.0, count: n_fft)
        for i in 0..<n_fft {
            win[i] = 0.5 - 0.5 * cos(2.0 * Double.pi * Double(i) / Double(n_fft))
        }
        return win
    }
    
    func yFrame(ypad: [Double]) -> [[Double]] {
        let n_frames = 1 + (ypad.count - n_fft) / hop_length
        
        let a1 = n_fft
        let a2 = n_frames
        
        var winFrames: [[Double]] = Array(repeating: [Double](repeating: 0.0, count: a2), count: a1)

        for i in 0..<n_fft {
            for j in 0..<n_frames {
                winFrames[i][j] = ypad[j * hop_length + i]
            }
        }
        return winFrames;
    }
    
    func median(valueInput: [Double]) -> Double {
        var values = valueInput
        values.sort()
        var median: Double
        
        let totalElements: Int = values.count
        
        if (totalElements % 2 == 0) {
            let sumOfMiddleElements: Double = values[totalElements / 2] + values[totalElements / 2 - 1]
            median = Double(sumOfMiddleElements) / 2
        } else {
            median = Double(values[values.count / 2])
        }
        return median
    }
    
    func powerToDb(melS: [[Double]]) -> [[Double]] {
        let a1 = melS.count
        let a2 = melS[0].count
        var log_spec: [[Double]] = Array(repeating: [Double](repeating: 0.0, count: a2), count: a1)
        
        var maxValue: Double = -100
        var ref_v: Double = 0
        
        let flattenMap = melS.flatMap{$0}
        ref_v = median(valueInput: flattenMap)
    
        
        var magnitude: Double = 0;
        
        for i in 0..<melS.count {
            for j in 0..<melS[0].count {
                magnitude = melS[i][j]
                log_spec[i][j] = 10.0 * log10(max(1E-10, magnitude))
                log_spec[i][j] -= 10.0 * log10(max(1E-10, ref_v))
                
                if (log_spec[i][j] > maxValue) {
                    maxValue = log_spec[i][j]
                }
            }
        }
        
        for i in 0..<melS.count {
            for j in 0..<melS[0].count {
                if (log_spec[i][j] < maxValue - 80.0) {
                    log_spec[i][j] = maxValue - 80.0
                }
            }
        }
        return log_spec
    }
    
    func dctFilter(n_filters: Int, n_input: Int) -> [[Double]] {
        let a1 = n_filters
        let a2 = n_input
        var basis: [[Double]] = Array(repeating: [Double](repeating: 0.0, count: a2), count: a1)
        
        var samples: [Double] = Array(repeating: 0.0, count: n_input)
        
        for i in 0..<n_input {
            let a = Double.pi / (2.0 * Double(n_input))
            samples[i] = Double(1 + 2 * i) * a
        }
        
        for j in 0..<n_input {
            basis[0][j] = 1.0 / sqrt(Double(n_input))
        }
        
        for i in 1..<n_filters {
            for j in 0..<n_input {
                basis[i][j] = cos(Double(i) * samples[j]) * sqrt(2.0 / Double(n_input))
            }
        }
        return basis
    }
    
    func melFilter() -> [[Double]] {
        let fftFreqs: [Double] = fftFreq()
        let melF: [Double] = melFreq(numMels: n_mels + 2)
        
        let fdiffSize = melF.count - 1
        var fdiff: [Double] = Array(repeating: 0.0, count: fdiffSize)
        
        for i in 0..<melF.count - 1 {
            fdiff[i] = melF[i + 1] - melF[i]
        }

        var ramps = [[Double]](repeating: [Double](repeating: 0, count: fftFreqs.count), count: melF.count)

        for i in 0..<melF.count {
            for j in 0..<fftFreqs.count {
                ramps[i][j] = melF[i] - fftFreqs[j]
            }
        }
        
        let a1 = n_mels
        let a2 = 1 + n_fft / 2
        var weights = [[Double]](repeating: [Double](repeating: 0, count: a2), count: a1)

        
        for i in 0..<n_mels {
            for j in 0..<fftFreqs.count {
                let lowerF: Double = -ramps[i][j] / fdiff[i]
                let upperF: Double = ramps[i + 2][j] / fdiff[i + 1]
                if (lowerF > upperF && upperF > 0) {
                    weights[i][j] = upperF;
                } else if (lowerF > upperF && upperF < 0) {
                    weights[i][j] = 0;
                } else if (lowerF < upperF && lowerF > 0) {
                    weights[i][j] = lowerF;
                } else if (lowerF < upperF && lowerF < 0) {
                    weights[i][j] = 0;
                } else {
                }
            }
        }
        
        var enorm: [Double] = Array(repeating: 0.0, count: n_mels)
        for i in 0..<n_mels {
            enorm[i] = 2.0 / (melF[i + 2] - melF[i])
            for j in 0..<fftFreqs.count {
                weights[i][j] *= enorm[i]
            }
        }
        return weights
    }
    
    func fftFreq() -> [Double] {
        let arraySize = 1 + n_fft / 2
        var freqs: [Double] = Array(repeating: 0.0, count: arraySize)
        for i in 0..<(1 + n_fft / 2 ) {
            freqs[i] = (sampleRate / 2) / Double(n_fft / 2) * Double(i)
        }
        return freqs
    }
    
    func melFreq(numMels: Int) -> [Double] {
        var LowFFreq: [Double] = Array(repeating: 0.0, count: 1)
        var HighFFreq: [Double] = Array(repeating: 0.0, count: 1)
        LowFFreq[0] = fMin
        HighFFreq[0] = fMax
        
        let melFLow: [Double] = freqToMel(freqs: LowFFreq)
        let melFHigh: [Double] = freqToMel(freqs: HighFFreq)
        
        var mels: [Double] = Array(repeating: 0.0, count: numMels)
        
        for i in 0..<numMels {
            let a = (melFHigh[0] - melFLow[0])
            mels[i] = melFLow[0] + a * Double(i) / Double((numMels - 1))
        }
        let result = melToFreq(mels: mels)
        return result
    }
    
    func melToFreqS(mels: [Double]) -> [Double] {
        var freqs = [Double]()
        for i in 0..<mels.count {
            freqs[i] = 700.0 * (pow(10, mels[i] / 2595.0) - 1.0)
        }
        return freqs
    }
    
    func freqToMelS(freqs: [Double]) -> [Double] {
        var mels = [Double]()
        for i in 0..<freqs.count {
            mels[i] = 2595.0 * log10(1.0 + freqs[i] / 700.0)
        }
        return mels
    }
    
    func melToFreq(mels: [Double]) -> [Double] {
        let f_min: Double = 0.0
        let f_sp: Double = 200.0 / 3
        
        let freqSize = mels.count
        var freqs: [Double] = Array(repeating: 0.0, count: freqSize)
        
        let min_log_hz: Double = 1000.0
        let min_log_mel: Double = (min_log_hz - f_min) / f_sp
        let logstep: Double = log(6.4) / 27.0
        
        for i in 0..<mels.count {
            if (mels[i] < min_log_mel) {
                freqs[i] = f_min + f_sp * mels[i]
            } else {
                freqs[i] = min_log_hz * exp(logstep * (mels[i] - min_log_mel))
            }
        }
        return freqs
    }
 
    func freqToMel(freqs: [Double]) -> [Double] {
        let f_min: Double = 0.0
        let f_sp: Double = 200.0 / 3
        
        let melsSize = freqs.count
        var mels: [Double] = Array(repeating: 0.0, count: melsSize)
        
        let min_log_hz: Double = 1000.0
        let min_log_mel: Double = (min_log_hz - f_min) / f_sp
        let logstep: Double = log(6.4) / 27.0
        
        for i in 0..<freqs.count {
            if (freqs[i] < min_log_hz) {
                mels[i] = (freqs[i] - f_min) / f_sp
            } else {
                mels[i] = min_log_mel + log(freqs[i] / min_log_hz) / logstep
            }
        }
        return mels
    }
}
